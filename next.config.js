/** @type {import('next').NextConfig} */
const nextConfig = {
    images:{
        domains :['cdn.imagin.studio']
    },
    typescript: {
        ignoreBuildErrors: true, // optional but highly recommended to keep the build working even if there
    }

}

module.exports = nextConfig
